using MySql.Data.MySqlClient;
using System.Data;

namespace GrixanVanekSuperBDApp
{
    public partial class Form1 : Form
    {
        private Database DB;
        private DataTable table;
        private MySqlDataAdapter adapter;

        class Database
        {
            public String serverString;
            public String databaseString;
            public String userString;
            public String passwordString;
            public String tableString;
            public String connectionString;

            MySqlConnection connection;

            public void buildConnectionString()
            {
                connectionString = "Server=" + serverString + "; Database=" + databaseString + "; User ID=" + userString +
                    "; Password =" + passwordString;
                connection = new MySqlConnection(connectionString);
            }

            public void openConnection()
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();
            }

            public void closeConnection()
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }

            public MySqlConnection GetConnection()
            {
                return connection;
            }
        }
        public Form1()
        {
            InitializeComponent();
            DB = new Database();
            table = new DataTable();
            adapter = new MySqlDataAdapter();
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            clearAll();
            DB.serverString = serverTextBox.Text;
            DB.databaseString = databaseTextBox.Text;
            DB.userString = userTextBox.Text;
            DB.passwordString = passwordTextBox.Text;
            DB.tableString = tableTextBox.Text;
            DB.buildConnectionString();
            DB.openConnection();
            string SQLCommand = "select * from " + DB.tableString + ";";
            MySqlCommand command = new MySqlCommand(SQLCommand, DB.GetConnection());
            adapter.SelectCommand = command;
            adapter.Fill(table);
            dataGridView1.DataSource = table;
            DB.closeConnection();
        }

        private void clearAll()
        {
            table = new DataTable();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            dataGridView1.EndEdit();
            DataTable changedRows = table.GetChanges();
            if (changedRows != null)
            {
                MySqlCommandBuilder builder = new MySqlCommandBuilder(adapter);
                adapter.Update(changedRows);
                table.AcceptChanges();
                MessageBox.Show("��������� ���������");
            }
        }

        private void findButton_Click(object sender, EventArgs e)
        {
            DB.openConnection();
            String findString = findTextBox.Text;
            MySqlCommand command = new MySqlCommand();
            String sqlCommand = "SELECT * FROM " + DB.databaseString + "." + DB.tableString + " WHERE ";
            String sqlParam;
            DataGridViewColumn col = dataGridView1.Columns[0];
            sqlParam = "@" + col.Name;
            sqlCommand += col.Name + "=" + sqlParam;
            if (col.ValueType == typeof(int))
                command.Parameters.Add(sqlParam, MySqlDbType.Int32).Value = Convert.ToInt32(findString);
            else
                command.Parameters.Add(sqlParam, MySqlDbType.VarChar).Value=findString;
            command.CommandText = sqlCommand;
            command.Connection=DB.GetConnection();
            adapter.SelectCommand = command;
            clearAll();
            adapter.Fill(table);
            dataGridView1.DataSource = table;
            DB.closeConnection();
        }
    }
}